const path = require('path')

const resolveClientModule = module =>
  path.resolve(__dirname, 'client/node_modules', module)

module.exports = {
  projects: [
    {
      displayName: 'client',
      modulePaths: ['<rootDir>/client/node_modules'],
      testMatch: ['**/client/**/__tests__/*-test.js'],
      setupTestFrameworkScriptFile: path.resolve(
        __dirname,
        'client/setupTests.js'
      ),
      transform: {
        '^.+\\.css$': resolveClientModule(
          'react-scripts/config/jest/cssTransform.js'
        ),
        '^.+\\.(js|jsx)$': resolveClientModule('babel-jest')
      }
    },
    {
      displayName: 'server',
      testMatch: ['**/server/**/__tests__/*-test.js']
    }
  ]
}
