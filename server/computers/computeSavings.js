const _ = require('lodash')

module.exports = function computeSavings (opts) {
  const {
    initialSavings,
    monthlySavings,
    interestRate,
    interestFrequency,
    monthsToCompute
  } = opts

  let currentSavings = initialSavings
  const history = []
  _.times(monthsToCompute + 1, month => {
    if (month === 0) {
      history.push(currentSavings)
    } else {
      currentSavings = currentSavings + monthlySavings
      const interestPayable = month % interestFrequency === 0
      if (interestPayable) {
        const timesPerYear = 12 / interestFrequency
        const interest = Math.floor(
          currentSavings * interestRate / timesPerYear
        )
        currentSavings = currentSavings + interest
      }
      history.push(currentSavings)
    }
  })
  return history
}
