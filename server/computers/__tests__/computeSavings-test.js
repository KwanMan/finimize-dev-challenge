const computeSavings = require('../computeSavings')

const testCases = [
  {
    interestFrequency: 1,
    expected: [10000, 11091, 12191, 13300, 14419, 15547, 16684]
  },
  {
    interestFrequency: 3,
    expected: [10000, 11000, 12000, 13325, 14325, 15325, 16733]
  },
  {
    interestFrequency: 12,
    // prettier-ignore
    expected: [ 10000, 11000, 12000, 13000, 14000, 15000, 16000, 17000, 18000, 19000, 20000, 21000, 24200 ]
  }
]

testCases.forEach(({ interestFrequency, expected }) => {
  test(`paying interest ${interestFrequency}`, () => {
    const history = computeSavings({
      initialSavings: 10000,
      monthlySavings: 1000,
      interestRate: 0.1,
      interestFrequency,
      monthsToCompute: expected.length - 1
    })
    expect(history).toEqual(expected)
  })
})
