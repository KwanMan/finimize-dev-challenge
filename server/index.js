const createServer = require('./createServer')

const port = process.env.PORT || 3001

createServer().listen(port, () => {
  console.log(`Server started on port ${port}`)
})
