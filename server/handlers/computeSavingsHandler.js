const Ajv = require('ajv')
const computeSavings = require('../computers/computeSavings')

const ajv = new Ajv()
const schema = {
  type: 'object',
  properties: {
    initialSavings: {
      type: 'integer'
    },
    monthlySavings: {
      type: 'integer'
    },
    interestRate: {
      type: 'number',
      minimum: 0
    },
    interestFrequency: {
      type: 'integer',
      minimum: 1
    },
    monthsToCompute: {
      type: 'integer',
      minimum: 0
    }
  },
  required: [
    'initialSavings',
    'monthlySavings',
    'interestRate',
    'interestFrequency',
    'monthsToCompute'
  ],
  additionalProperties: false
}

module.exports = function computeSavingsHandler (req, res, next) {
  const valid = ajv.validate(schema, req.body)
  if (!valid) {
    return res.status(400).json(ajv.errors)
  }
  const savings = computeSavings(req.body)
  res.json(savings)
}
