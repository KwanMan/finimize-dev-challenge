const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const cors = require('cors')
const computeSavingsHandler = require('./handlers/computeSavingsHandler')

const distPath = path.resolve(__dirname, '../client/build')

module.exports = function createServer () {
  const app = express()

  app.use(cors())
  app.use(bodyParser.json())

  // Express only serves static assets in production
  if (process.env.NODE_ENV === 'production') {
    app.use(express.static(distPath))
  }

  app.post('/compute-savings', computeSavingsHandler)

  return app
}
