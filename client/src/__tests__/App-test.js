import React from 'react'
import { shallow } from 'enzyme'
import App from '../App'
import Projection from '../components/Projection'

jest.mock('../api')

const apiMock = require('../api')

afterEach(() => {
  apiMock.__reset()
})

it('renders without crashing', () => {
  expect(() => shallow(<App />)).not.toThrow()
})

it('calls api on mount', () => {
  shallow(<App />)
  expect(apiMock.calculateSavings).toHaveBeenCalledTimes(1)
})

it('renders the graph', () => {
  const app = shallow(<App />)
  expect(app.find(Projection)).toHaveLength(1)
})
