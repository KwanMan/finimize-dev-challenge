import React, { Component } from 'react'
import { debounce, pick } from 'lodash'
import CurrencyInput from './components/CurrencyInput'
import SliderInput from './components/SliderInput'
import RadioSelector from './components/RadioSelector'
import Projection from './components/Projection'
import { calculateSavings } from './api'
import './App.css'

const interestFrequencies = ['Monthly', 'Quarterly', 'Annually']
const keysAffectingProjection = [
  'initialSavings',
  'monthlySavings',
  'interestRate',
  'interestFrequency'
]

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      initialSavings: 10000,
      monthlySavings: 100,
      interestRate: 0.06,
      interestFrequency: 'Quarterly',
      projectionYears: 5,
      projection: [],
      req: 0
    }
    this.updateSavings = debounce(this.updateSavings, 200, {
      leading: true,
      trailing: true
    })
  }

  async updateSavings () {
    const req = this.state.req + 1
    this.setState({ loading: true, req })
    const projection = await calculateSavings(
      pick(this.state, keysAffectingProjection)
    )
    if (this.state.req === req) {
      this.setState({ projection })
    }
  }

  componentDidUpdate (prevProps, prevState) {
    const changed = keysAffectingProjection.some(
      key => prevState[key] !== this.state[key]
    )
    if (changed) {
      this.updateSavings()
    }
  }

  componentDidMount () {
    this.updateSavings()
  }
  render () {
    const {
      initialSavings,
      monthlySavings,
      interestRate,
      interestFrequency,
      projectionYears,
      projection
    } = this.state
    return (
      <div className='App'>
        <div className='header-banner'>
          <h1 className='fmz-white-font container'>
            Finimize Interest Rate Calculator
          </h1>
        </div>
        <div className='container'>
          <div className='row'>
            <div className='financial-inputs col-xs-12 col-lg-4'>
              <p className='input-label'>How often is interest paid?</p>
              <RadioSelector
                options={interestFrequencies}
                value={interestFrequency}
                onChange={v => this.setState({ interestFrequency: v })}
              />

              <p className='input-label'>How much have you saved?</p>
              <CurrencyInput
                value={initialSavings}
                onChange={v => this.setState({ initialSavings: v })}
              />

              <p className='input-label'>How much will you save each month?</p>
              <CurrencyInput
                value={monthlySavings}
                onChange={v => this.setState({ monthlySavings: v })}
              />

              <p className='input-label'>
                How much interest will you earn per year?
              </p>
              <SliderInput
                value={Number((interestRate * 100).toFixed(2))}
                onChange={v => this.setState({ interestRate: v / 100 })}
                unit='%'
                min={0}
                max={10}
                step={0.25}
              />

              <p className='input-label'>
                How much years do you want to project?
              </p>
              <SliderInput
                value={projectionYears}
                onChange={v => this.setState({ projectionYears: Number(v) })}
                unit=' Years'
                min={1}
                max={50}
                step={1}
              />
            </div>
            <div className='col-xs-12 col-lg-8'>
              <Projection
                projection={projection
                  .slice(0, projectionYears * 12 + 1)
                  .map((savings, month) => ({
                    month,
                    savings
                  }))}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default App
