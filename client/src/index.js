import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import 'flexboxgrid2'
import './index.css'

ReactDOM.render(<App />, document.getElementById('root'))
