import axios from 'axios'
import memoize from 'p-memoize'

const frequencyMap = {
  monthly: 1,
  quarterly: 3,
  annually: 12
}

function toPence (pounds) {
  return Math.floor(pounds * 100)
}

export const calculateSavings = memoize(async function ({
  initialSavings,
  monthlySavings,
  interestRate,
  interestFrequency
}) {
  const { data } = await axios.post('http://localhost:3001/compute-savings', {
    initialSavings: toPence(initialSavings),
    monthlySavings: toPence(monthlySavings),
    interestRate,
    interestFrequency: frequencyMap[interestFrequency.toLowerCase()],
    monthsToCompute: 50 * 12
  })
  return data
})
