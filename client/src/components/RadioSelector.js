import React from 'react'
import PropTypes from 'prop-types'
import './RadioSelector.css'

export default function RadioSelector ({ options, value, onChange }) {
  return (
    <div className='RadioSelector'>
      {options.map(option => (
        <div key={option}>
          <input
            name={option}
            value={option}
            type='radio'
            checked={option === value}
            onChange={e => onChange(e.currentTarget.value)}
          />
          <span>{option}</span>
        </div>
      ))}
    </div>
  )
}

const valueType = PropTypes.oneOfType([PropTypes.number, PropTypes.string])

RadioSelector.propTypes = {
  options: PropTypes.arrayOf(valueType).isRequired,
  value: valueType.isRequired,
  onChange: PropTypes.func
}

RadioSelector.defaultProps = {
  onChange: function noop () {}
}
