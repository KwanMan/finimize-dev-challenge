import React from 'react'
import PropTypes from 'prop-types'
import './SliderInput.css'

export default function SliderInput ({ value, onChange, unit, min, max, step }) {
  return (
    <div className='fmz-slider'>
      <p>{value}{unit}</p>
      <input
        type='range'
        value={value}
        min={min}
        max={max}
        step={step}
        onChange={e => onChange(e.target.value)}
      />
    </div>
  )
}

SliderInput.propTypes = {
  value: PropTypes.number.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  step: PropTypes.number.isRequired,
  onChange: PropTypes.func,
  unit: PropTypes.string
}

SliderInput.defaultProps = {
  onChange: function noop () {}
}
