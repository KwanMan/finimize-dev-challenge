import React from 'react'
import PropTypes from 'prop-types'
import './CurrencyInput.css'

export default function CurrencyInput ({ value, onChange }) {
  return (
    <div className='currency-input'>
      <span>£</span>
      <input
        type='number'
        value={value}
        onChange={e => onChange(Number(e.target.value))}
      />
    </div>
  )
}

CurrencyInput.propTypes = {
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func
}

CurrencyInput.defaultProps = {
  onChange: function noop () {}
}
