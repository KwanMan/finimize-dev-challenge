import React from 'react'
import PropTypes from 'prop-types'
import { Group } from '@vx/group'
import { scaleTime, scaleLinear } from '@vx/scale'
import { AreaClosed } from '@vx/shape'
import { AxisLeft, AxisBottom } from '@vx/axis'
import { LinearGradient } from '@vx/gradient'
import { withParentSize } from '@vx/responsive'
import { GridRows } from '@vx/grid'
import { extent } from 'd3-array'
import addMonths from 'date-fns/add_months'

function Projection ({ projection, parentWidth }) {
  const width = parentWidth
  const height = Math.floor(width * 0.7)

  const margin = {
    top: 60,
    bottom: 60,
    left: 80,
    right: 0
  }
  const xMax = width - margin.left - margin.right
  const yMax = height - margin.top - margin.bottom
  const now = new Date()
  const x = d => addMonths(now, d.month)
  const y = d => d.savings / 100

  const xScale = scaleTime({
    range: [0, xMax],
    domain: extent(projection, x)
  })
  const yScale = scaleLinear({
    range: [yMax, 0],
    domain: extent(projection, y)
  })
  return (
    <div>
      <svg width={width} height={height}>
        <LinearGradient from='#fce38a' to='#f38181' id='gradient' />

        <GridRows
          top={margin.top}
          left={margin.left}
          scale={yScale}
          numTicks={5}
          width={xMax}
        />
        <Group top={margin.top} left={margin.left}>

          <AreaClosed
            data={projection}
            xScale={xScale}
            yScale={yScale}
            x={x}
            y={y}
            fill={'url(#gradient)'}
            stroke={''}
          />

          <AxisLeft
            scale={yScale}
            top={0}
            left={0}
            label={'Savings (£)'}
            stroke={'#1b1a1e'}
            tickTextFill={'#1b1a1e'}
            labelProps={{ fontSize: 10, fill: 'black', dx: '-1em' }}
          />

          <AxisBottom
            scale={xScale}
            top={yMax}
            label={'Year'}
            stroke={'#1b1a1e'}
            tickTextFill={'#1b1a1e'}
          />

        </Group>
      </svg>
    </div>
  )
}

Projection.propTypes = {
  projection: PropTypes.arrayOf(
    PropTypes.shape({
      month: PropTypes.number,
      savings: PropTypes.number
    })
  ).isRequired,
  parentWidth: PropTypes.number.isRequired
}

export default withParentSize(Projection)
